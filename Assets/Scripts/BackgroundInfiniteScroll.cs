﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundInfiniteScroll : MonoBehaviour
{    
    [SerializeField]
    private float m_BaseFrequency = 0.3f;

    private Renderer mCachedRenderer;

    private float mVariableFrequency = 0;


    public float VariableFrequency 
    {
        set
        {
            mVariableFrequency = value;
        }
    }

    // Start is called before the first frame update
    void Awake()
    {
        mCachedRenderer = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if(mCachedRenderer && mVariableFrequency > 0)
        {
            mCachedRenderer.material.mainTextureOffset = Vector3.right * mVariableFrequency * (m_BaseFrequency/ 100) * Time.time ;
        }
    }
}
