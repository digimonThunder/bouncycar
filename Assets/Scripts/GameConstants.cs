﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameConstants : MonoBehaviour
{
    public const float kMaxCarSpeed = 30;

    public const float kMinCarSpeed = 15;

    public const float kForwardZLimit = 3;

    public const float kBackwardZLimit = 0;



}
