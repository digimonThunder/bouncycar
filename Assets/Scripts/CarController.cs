﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
    #region PRIVATE_SERIALIZED_MEMBERS

    [SerializeField]
    private Transform m_CarTransform;

    [SerializeField]
    private Transform m_FrontWheel;

    [SerializeField]
    private Transform m_RearWheel;

    [SerializeField]
    private Transform m_RoadTransform;

    [SerializeField]
    private BackgroundInfiniteScroll[] m_BackgroundInfiniteScroll;

    [SerializeField]
    private Animator m_CarJumpAnimator;

    [SerializeField]
    private Camera m_Camera;

    [SerializeField]
    private UnityEngine.UI.Text m_CountDownText;


    #endregion //PRIVATE_SERIALIZED_MEMBERS



    #region PRIVATE_MEMBERS

    private float mSpeed = 20;

    private float mMaxHorizonatalBoundary;

    private float mMaxVerticalUpBoundary;

    private float mMaxVerticalDownBoundary;

    private Vector3 mRefVector;

    private bool mEntryCompletd = false;

    #endregion //PRIVATE_MEMBERS


    // Start is called before the first frame update
    void Start()
    {
        SetUpRoadBoundaryValues();

        StartCoroutine(PlayCountDown());
    }

    IEnumerator PlayCountDown()
    {
        m_CountDownText.enabled = true;

        WaitForSeconds wait = new WaitForSeconds(1f);

        for(int i=3;i >= 1 ;i--)
        {
            yield return wait;

            m_CountDownText.text = i.ToString();
        }

        yield return wait;

        m_CountDownText.enabled = false;

        StartCoroutine(OnJumpAnimationComplete());

    }

    IEnumerator OnJumpAnimationComplete()
    {
        m_CarJumpAnimator.enabled = true;

        yield return new WaitForSeconds(0.7f);

        m_CarJumpAnimator.enabled = false;

        mEntryCompletd = true;
    }

    public float GetScreenWidth
    {
        get
        {
            float height = m_Camera.orthographicSize * 2.0f;

            float width = height * Camera.main.aspect;

            return width;
        }
    }


    void SetUpRoadBoundaryValues()
    {
        float halfHeight = m_RoadTransform.localScale.y / 2;

        float halfWidth = GetScreenWidth / 2;
    
        mMaxHorizonatalBoundary = m_RoadTransform != null ? halfWidth - (halfWidth * 0.15f) : 0; //15% ignored in halfwidth as offset 

        mMaxVerticalUpBoundary = m_RoadTransform != null ? halfHeight - (halfHeight * 0.02f) : 0; //similar height offset 
        
        mMaxVerticalDownBoundary = m_RoadTransform != null ? halfHeight - (halfHeight * 0.8f) : 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (mEntryCompletd && Input.GetMouseButton(0))
        {
            Ray posRay = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;

            if (Physics.Raycast(posRay, out hit, 100))
            {
                UpdatCarPositionAndSpeed(hit.point);
            }

        }

        if (mEntryCompletd)
        {
            UpdateBgScrollSpeed(mSpeed);
        }
        else
        {
            UpdateBgScrollSpeed(0);
        }

        RotateWheels(mSpeed);
    }

    //Updates the car position and speed based on the clicked position 
    void UpdatCarPositionAndSpeed(Vector3 inClickedPos)
    {
        float x = Mathf.Clamp(inClickedPos.x, -mMaxHorizonatalBoundary, mMaxHorizonatalBoundary);
        float y = Mathf.Clamp(inClickedPos.y, -mMaxVerticalDownBoundary, mMaxVerticalUpBoundary);
        float z = Mathf.Clamp(inClickedPos.y, GameConstants.kBackwardZLimit, GameConstants.kForwardZLimit);

        mRefVector.x = x < 0 ? x - y  : x + y;
        mRefVector.y = y;
        mRefVector.z = z;

        mSpeed = x < 0 ? GameConstants.kMinCarSpeed : GameConstants.kMaxCarSpeed;

        float distance = Vector3.Distance(m_CarTransform.position, mRefVector);

        float timeSpeed = Time.deltaTime * mSpeed / distance;

        m_CarTransform.position = Vector3.Lerp(m_CarTransform.position, mRefVector, timeSpeed);
    }


    //Updates rotates the wheel based on the current speed
    void RotateWheels(float inAngleSpeed)
    {
        m_FrontWheel.Rotate(Vector3.forward * inAngleSpeed * Time.deltaTime * mSpeed);

        m_RearWheel.Rotate(Vector3.forward * inAngleSpeed * Time.deltaTime * mSpeed);
    }

    void UpdateBgScrollSpeed(float inSpeed)
    {
        foreach (BackgroundInfiniteScroll controller in m_BackgroundInfiniteScroll)
        {
            controller.VariableFrequency = inSpeed;
        }
    }
}
